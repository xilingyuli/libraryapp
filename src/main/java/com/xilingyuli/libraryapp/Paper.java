package com.xilingyuli.libraryapp;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by xilingyuli on 2015/12/10.
 * 对应后台paper，表示一篇文章
 */
public class Paper {
    private String id,title,paperClass,originalFile;
    private String[] authors;
    private String[] images;
    private String[] keyWords;
    private Volume volume;
    Paper(String id) { this.id = id; }
    public String getId() { return id; }
    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }
    public void setPaperClass(String paperClass) { this.paperClass = paperClass; }
    public String getPaperClass() { return paperClass; }
    public void setOriginalFile(String originalFile) { this.originalFile = originalFile; }
    public String getOriginalFile() { return originalFile; }
    public void setAuthors(String[] authors) { this.authors = authors; }
    public String[] getAuthors() { return authors; }
    public void setImages(String[] images) { this.images = images; }
    public String[] getImages() { return images; }
    public void setKeyWords(String[] keyWords) { this.keyWords = keyWords; }
    public String[] getKeyWords() { return keyWords; }
    public void setVolume(Volume volume) { this.volume = volume; }
    public Volume getVolume() { return volume; }

    //根据xml字符串解析Paper对象
    public static Paper decodeFromXml(String xml)
    {
        Document doc = XmlDeal.getXml(xml);
        if(doc==null)
            return null;
        Element root = doc.getDocumentElement();
        return decodeFromXml(root);
    }

    //根据根节点解析Paper对象
    public static Paper decodeFromXml(Element root)
    {
        if(root==null||!root.getTagName().equals("Paper"))
            return null;
        NodeList nl = root.getChildNodes();

        Paper paper = new Paper(nl.item(1).getTextContent());
        paper.setAuthors(getStrings(((Element) nl.item(0)).getElementsByTagName("Name")));
        paper.setImages(getStrings(((Element) nl.item(2)).getElementsByTagName("FullUri")));
        paper.setKeyWords(getStrings(((Element) nl.item(3)).getElementsByTagName("Name")));
        String[] originalFile = getStrings(((Element) nl.item(4)).getElementsByTagName("FullUri"));
        if(originalFile.length>0)
            paper.setOriginalFile(originalFile[0]);
        String[] paperClass = getStrings(((Element) nl.item(5)).getElementsByTagName("Name"));
        if(paperClass.length>0)
            paper.setPaperClass(paperClass[0]);
        paper.setTitle(nl.item(6).getTextContent());
        paper.setVolume(Volume.decodeFromXml((Element)nl.item(7)));
        return paper;
    }

    //根据xml字符串解析Paper对象数组
    public static Paper[] decodePapersFromXml(String xml)
    {
        Document doc = XmlDeal.getXml(xml);
        if(doc==null)
            return null;
        Element root = doc.getDocumentElement();
        NodeList nl = root.getElementsByTagName("Paper");
        Paper[] papers = new Paper[nl.getLength()];
        for(int i=0;i<nl.getLength();i++)
            papers[i] = decodeFromXml((Element)nl.item(i));
        return papers;
    }

    //根据xml字符串拆分Paper对象数组
    public static String[] splitPapersFromXml(String xml)
    {
        Document doc = XmlDeal.getXml(xml);
        if(doc==null)
            return null;
        Element root = doc.getDocumentElement();
        NodeList nl = root.getChildNodes();
        String[] s = new String[nl.getLength()];
        for(int i=0;i<nl.getLength();i++)
            s[i] = XmlDeal.getString(nl.item(i));
        return s;
    }

    //将nodeList转化成String数组，供上边的方法使用
    private static String[] getStrings(NodeList nl)
    {
        String[] s = new String[nl.getLength()];
        for(int i=0;i<nl.getLength();i++)
            s[i] = nl.item(i).getTextContent();
        return s;
    }
}
