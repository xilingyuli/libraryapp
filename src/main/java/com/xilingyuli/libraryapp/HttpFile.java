package com.xilingyuli.libraryapp;

import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by xilingyuli on 2015/12/21.
 */
public class HttpFile extends Thread{

    private String connStr;  //连接字符串
    private String path;  //存储路径
    private String name;
    private Toast toast;

    HttpFile(String connStr,String name, String path, Toast toast)
    {
        this.connStr = connStr;
        this.name = name;
        this.path = path;
        this.toast = toast;
    }

    public void run()
    {
        if(connStr==null||connStr.equals(""))
            return;
        try {

            URL url = new URL(connStr);

            //设置参数
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setConnectTimeout(5000);
            con.setUseCaches(false);
            con.setRequestMethod("GET");

            //处理返回的结果
            if(con.getResponseCode()==HttpURLConnection.HTTP_OK) {

                //打开文件
                path = Environment.getExternalStorageDirectory().getPath()+"/"+path;
                File p = new File(path);
                if(!p.exists())
                    p.mkdir();
                File file = new File(path+"/"+name);
                file.createNewFile();

                //向其中写入数据
                FileOutputStream os = new FileOutputStream(file);
                byte buffer[] = new byte[1024];
                InputStream is = con.getInputStream();
                int count = -1;
                while ((count = is.read(buffer, 0, buffer.length)) != -1)
                    os.write(buffer, 0, count);
                is.close();

                toast.show();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            toast.setText("保存失败");
            toast.show();
        }
    }
}
