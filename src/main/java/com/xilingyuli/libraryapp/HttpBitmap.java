package com.xilingyuli.libraryapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Message;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by xilingyuli on 2015/12/4.
 */
public class HttpBitmap extends Thread{
    private String connStr;  //连接字符串
    private Message response;  //返回的数据

    HttpBitmap(String connStr, Message response)
    {
        this.connStr = connStr;
        this.response = response;
    }

    public void run()
    {
        if(connStr==null||connStr.equals("")||response==null)
            return;
        try {
            URL url = new URL(connStr);

            //设置参数
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setConnectTimeout(5000);
            con.setUseCaches(false);

            //设置header
            //con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            //处理返回的结果
            /*
            * 若请求码不是200，则返回；若请求码是200，请求成功，则封装数据。
            * 若出现异常，则数据key值为"error"，value为异常详细信息
            */
            response.what = con.getResponseCode();
            if(response.what==HttpURLConnection.HTTP_OK)
            {
                InputStream is = con.getInputStream();
                Bundle b = new Bundle();
                BitmapFactory.Options opt = new BitmapFactory.Options();
                opt.inPreferredConfig = Bitmap.Config.RGB_565;
                opt.inSampleSize = 2;
                b.putParcelable("img", BitmapFactory.decodeStream(is,null,opt));
                is.close();
                response.setData(b);
            }
            con.disconnect();
            response.sendToTarget();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            //处理异常
            Bundle bd = new Bundle();
            bd.putString("error",e.getMessage());
            response.setData(bd);
            response.sendToTarget();
        }
    }
}
