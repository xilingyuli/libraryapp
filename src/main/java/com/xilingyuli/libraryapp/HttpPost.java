package com.xilingyuli.libraryapp;

import android.os.Bundle;
import android.os.Message;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by xilingyuli on 2015/11/26.
 */
public class HttpPost extends Thread{

    private String connStr;  //连接字符串
    private String method;  //方法名
    private String data;  //发送的数据
    private Message response;  //返回的数据

    HttpPost(String connStr, String method, String data, Message response)
    {
        this.connStr = connStr;
        this.method = method;
        this.data = data;
        this.response = response;
    }

    public void run()
    {
        if(connStr==null||connStr.equals("")||data==null)
            return;
        try {

            byte b[] = data.getBytes("utf-8");

            String urlString = connStr;
            if(method!=null)
                urlString += "/"+method;
            URL url = new URL(urlString);

            //设置参数
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setConnectTimeout(5000);
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");

            //设置header
            con.setRequestProperty("Content-Type", "application/xml");

            //写入数据
            OutputStream os = con.getOutputStream();
            os.write(b);
            os.flush();
            os.close();

            if(response==null)
                return;

            //处理返回的结果
            /*
            * message.what是请求码，若请求码不是200，则返回
            * 若请求码是200，请求成功，则封装数据。有方法名时数据key值为方法名，否则key值为"data"
            * 若出现异常，则数据key值为"error"，value为异常详细信息
            */
            response.what = con.getResponseCode();
            if(response.what==HttpURLConnection.HTTP_OK)
            {
                //向bos写入返回的数据
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte buffer[] = new byte[1024];
                InputStream is = con.getInputStream();
                int count = -1;
                while ((count = is.read(buffer, 0, buffer.length)) != -1)
                    bos.write(buffer, 0, count);
                is.close();

                //封装数据
                Bundle bd = new Bundle();
                if(method!=null)
                    bd.putString(method, bos.toString("utf-8"));
                else
                    bd.putString("data", bos.toString("utf-8"));
                response.setData(bd);
            }
            con.disconnect();
            response.sendToTarget();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            //处理异常
            if(response==null)
                return;
            Bundle bd = new Bundle();
            bd.putString("error",e.getMessage());
            response.setData(bd);
            response.sendToTarget();
        }
    }
}
