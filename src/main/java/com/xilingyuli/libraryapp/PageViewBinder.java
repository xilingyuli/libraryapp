package com.xilingyuli.libraryapp;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

/**
 * Created by xilingyuli on 2015/12/8.
 * 为inmageView设置bitmap资源，供adapter使用
 */
public class PageViewBinder implements SimpleAdapter.ViewBinder {
    @Override
    public boolean setViewValue(View view, Object data, String textRepresentation) {
        if((view instanceof ImageView)&(data instanceof Bitmap))
        {
            ImageView iv = (ImageView)view;
            Bitmap bmp = (Bitmap)data;
            iv.setAdjustViewBounds(true);
            iv.setImageBitmap(bmp);
            return true;
        }
        return false;
    }
}
