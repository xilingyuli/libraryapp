package com.xilingyuli.libraryapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import android.widget.ImageView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.net.HttpURLConnection;

public class LoginCoverActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGHT = 3000; //延迟三秒
    private ImageView imageView;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpURLConnection.HTTP_OK) {
                Bundle bd = msg.getData();
                if (bd.containsKey("SplashScreen"))
                {
                    try {
                        Document doc = XmlDeal.getXml(bd.getString("SplashScreen"));
                        Element root = doc.getDocumentElement();
                        String uri = root.getElementsByTagName("FullUri").item(0).getTextContent();
                        new HttpBitmap(getString(R.string.root_resource)+uri,imgHandler.obtainMessage()).start();
                        return;
                    }catch (Exception e) {}
                }
            }
            show();
        }
    };
    private Handler imgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpURLConnection.HTTP_OK) {
                Bundle bd = msg.getData();
                Parcelable p = bd.getParcelable("img");
                if (p != null) {
                    imageView.setImageBitmap((Bitmap)p);
                }
            }
            show();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_cover);
        imageView = (ImageView)findViewById(R.id.imageView2);
        new HttpGet(getString(R.string.root_url),"SplashScreen","current=true",handler.obtainMessage()).start();
        Intent intent = new Intent().setClass(this, MyService.class);
        startService(intent);
    }
    private void show()
    {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(LoginCoverActivity.this,MainActivity.class);
                LoginCoverActivity.this.startActivity(mainIntent);
                LoginCoverActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
    }
}
