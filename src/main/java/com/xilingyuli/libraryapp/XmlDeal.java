package com.xilingyuli.libraryapp;

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by xilingyuli on 2015/11/3.
 * xml解析转化工具
 */

public class XmlDeal {

    //将xml字符串解析成dom对象
    public static Document getXml(String s)
    {
        try {
            s = StringEscapeUtils.unescapeHtml4(s);  //反转义，位置待调整
            //s = s.replaceAll(">\\s+<", "><");
            InputSource is = new InputSource(new StringReader(s));
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
            return doc;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //将xml节点转成字符串
    public static String getString(Node e)
    {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            OutputStream os = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(e),new StreamResult(os));
            return os.toString();
        }catch (Exception ec)
        {
            ec.printStackTrace();
            return null;
        }
    }
}

