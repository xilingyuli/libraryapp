package com.xilingyuli.libraryapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private GridView gridView;

    private Volume[] volumes;
    private List<Map<String, Object>> data;  //gridView源数据

    private int num;  //统计已接收到图片资源的张数
    private boolean isConnect;  //记录网络连接状态

    //处理文章列表、期刊列表的http请求数据
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpURLConnection.HTTP_OK) {
                Bundle bd = msg.getData();
                if(bd.containsKey("Paper"))  //请求的是文章列表
                {
                    //跳转到显示文章列表的页面
                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this,ArticleListActivity.class).putExtra("ArrayOfPaper",bd.getString("Paper"));
                    startActivity(intent);
                }
                else if(bd.containsKey("Volume"))  //请求的是期刊列表
                {
                    volumes = Volume.decodeVolumesFromXml(bd.getString("Volume"));
                    setListData();
                }
                else if(bd.containsKey("FeedBack"))  //发送反馈
                {
                    Toast.makeText(MainActivity.this,"谢谢您，我们已收到您的反馈",Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(MainActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
            }
        }
    };

    //处理获取的期刊封面图片资源
    private Handler imgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            num++;
            if (msg.what == HttpURLConnection.HTTP_OK) {
                Bundle bd = msg.getData();
                Parcelable p = bd.getParcelable("img");
                if(p!=null)
                {
                    Map<String, Object> map =  data.get(msg.arg1);
                    map.put("img", p);
                    data.set(msg.arg1,map);
                }
                else
                {
                    //获取图片失败
                    Toast.makeText(MainActivity.this,"无法加载第"+msg.arg1+"期期刊封面",Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                //网络连接失败
                isConnect = false;
            }

            //所有页面图片加载完毕
            if(num==volumes.length)
            {
                //将图片显示出来
                ((SimpleAdapter)gridView.getAdapter()).notifyDataSetChanged();

                //网络请求失败，进行提示
                if(!isConnect)
                    Toast.makeText(MainActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SearchView searchView = (SearchView)findViewById(R.id.searchView);
        Button button = (Button)findViewById(R.id.button);

        gridView = (GridView)findViewById(R.id.gridView);
        View emptyView = findViewById(R.id.empty);
        gridView.setEmptyView(emptyView);
        data = new ArrayList<Map<String, Object>>();
        SimpleAdapter adapter = new SimpleAdapter(
                this,
                this.data,
                R.layout.listitem_page,
                new String[] {"img"},
                new int[] { R.id.imageView});
        adapter.setViewBinder(new PageViewBinder());
        gridView.setAdapter(adapter);

        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setSelection(Calendar.getInstance().get(Calendar.YEAR) - 2014);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //请求期刊列表
                new HttpGet(getString(R.string.root_url), "Volume", "year=" + (2014 + i), handler.obtainMessage()).start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //全文搜索功能
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                try {
                    if (!"".equals(s)) {
                        new HttpGet(getString(R.string.root_url), "Paper", "words=" + java.net.URLEncoder.encode(s, "utf-8"), handler.obtainMessage()).start();
                    }
                }catch (Exception e){}
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String s = searchView.getQuery() + "";
                    if (!"".equals(s)) {
                        new HttpGet(getString(R.string.root_url), "Paper", "words=" + java.net.URLEncoder.encode(s, "utf-8"), handler.obtainMessage()).start();
                    }
                }catch (Exception e){}
            }
        });

        //点击期刊封面则跳转至文章列表
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new HttpGet(getString(R.string.root_url), "Paper", "volumeId=" + volumes[i].getId(), handler.obtainMessage()).start();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.collectItem:  //跳转至收藏夹列表
                startActivity(new Intent().setClass(MainActivity.this,ArticleCollectedListActivity.class));
                break;
            case R.id.feedbackItem:  //跳出反馈对话框
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                final EditText editText1 = new EditText(MainActivity.this);
                editText1.setHint("请写下您对我们的意见或建议");
                builder.setView(editText1);
                builder.setTitle("联系我们");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            new HttpPost(getString(R.string.root_url), "FeedBack",
                                    "<FeedBackPost xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.datacontract.org/2004/07/EssayBackEnd.Models\"><content>"
                                            + java.net.URLEncoder.encode(editText1.getText() + "", "utf-8")
                                            + "</content></FeedBackPost>",
                                    handler.obtainMessage()).start();
                        }catch (Exception e){}
                    }
                });
                builder.setNegativeButton("取消",null);
                builder.show();
                break;
            case R.id.aboutItem:  //跳出关于对话框
                AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                View view = View.inflate(this,R.layout.about_message,null);
                builder2.setView(view);
                builder2.setTitle("关于我们");
                builder2.setPositiveButton("确定", null);
                builder2.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //根据volumes对象获取封面图片
    private void setListData()
    {
        if(volumes==null)
            return;
        data.clear();
        ((SimpleAdapter)gridView.getAdapter()).notifyDataSetChanged();
        for(Volume v : volumes)
            data.add(new HashMap<String, Object>());
        num = 0;
        isConnect = true;
        for(int i=0;i<volumes.length;i++) {
            new HttpBitmap(getString(R.string.root_resource)+ volumes[i].getCoverImage(),imgHandler.obtainMessage(-1,i,0)).start();
        }
    }
}
