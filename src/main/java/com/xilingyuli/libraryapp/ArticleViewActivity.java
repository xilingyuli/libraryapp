package com.xilingyuli.libraryapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.w3c.dom.Element;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleViewActivity extends AppCompatActivity {

    private ListView listView;  //文章页面列表
    private SharedPreferences sharedPreferences;  //收藏夹

    private String id;
    private Paper paper;  //文章
    private List<Map<String, Object>> data;  //页面图片，作为列表资源

    private int num;
    private boolean isConnect;  //记录网络连接状态

    //处理获取到的页面图片
    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            num++;
            if (msg.what == HttpURLConnection.HTTP_OK) {
                Bundle bd = msg.getData();
                Parcelable p = bd.getParcelable("img");
                if(p!=null)
                {
                    Map<String, Object> map =  data.get(msg.arg1);
                    map.put("img", p);
                    data.set(msg.arg1, map);
                }
                else
                {
                    //获取图片失败
                    Toast.makeText(ArticleViewActivity.this,"无法获取文章第"+msg.arg1+"页，请重试",Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                //网络连接失败
                isConnect = false;
            }

            //所有页面图片加载完毕
            if(num==paper.getImages().length)
            {
                ((SimpleAdapter)listView.getAdapter()).notifyDataSetChanged();
                //网络请求失败，进行提示
                if(!isConnect)
                    Toast.makeText(ArticleViewActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
            }
        }
    };

    //处理根据id获取到的Paper
    private Handler urlHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bd = msg.getData();
            if (msg.what == HttpURLConnection.HTTP_OK && bd.containsKey(id)) {
                try {
                    Element e = XmlDeal.getXml(bd.getString(id)).getDocumentElement();  //xml根元素
                    paper = Paper.decodeFromXml(e);
                    setListData();
                }catch (Exception e)
                {
                    Toast.makeText(ArticleViewActivity.this, "无法解析该文章，可能该文章已被删除", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(ArticleViewActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_view);
        sharedPreferences = getSharedPreferences("collect", Context.MODE_PRIVATE);

        listView = (ListView) findViewById(R.id.listView);
        View emptyView = findViewById(R.id.empty);
        listView.setEmptyView(emptyView);
        data = new ArrayList<Map<String, Object>>();
        SimpleAdapter adapter = new SimpleAdapter(
                this,
                this.data,
                R.layout.listitem_page,
                new String[] {"img"},
                new int[] { R.id.imageView});
        adapter.setViewBinder(new PageViewBinder());
        listView.setAdapter(adapter);

        Intent intent = getIntent();
        if (intent.hasExtra("Paper")) {
            paper = Paper.decodeFromXml(intent.getStringExtra("Paper"));
            if(paper!=null)
                id = paper.getId();
            setListData();
        } else if (intent.hasExtra("Id")){
            id = intent.getStringExtra("Id");
            getPaper();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_collect, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.collectItem:
                if(paper!=null && !"".equals(paper.getId()) && !"".equals(paper.getTitle())) {
                    if (sharedPreferences.contains(paper.getId()))  //取消收藏
                    {
                        sharedPreferences.edit().remove(paper.getId()).commit();
                        item.setIcon(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                    } else  //收藏
                    {
                        sharedPreferences.edit().putString(paper.getId(), paper.getTitle()).commit();
                        item.setIcon(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                    }
                }
                getWindow().invalidatePanelMenu(Window.FEATURE_OPTIONS_PANEL);
                break;
            case R.id.downloadItem:
                new HttpFile(getString(R.string.root_resource)+paper.getOriginalFile(),
                        paper.getTitle()+".pdf",
                        getString(R.string.app_name),
                        Toast.makeText(this,"已保存至"+getString(R.string.app_name)+"目录",Toast.LENGTH_LONG))
                        .start();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (sharedPreferences.contains(id))
            menu.findItem(R.id.collectItem).setIcon(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
        else
            menu.findItem(R.id.collectItem).setIcon(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
        urlHandler.removeCallbacksAndMessages(null);
        try{
            for(Map<String,Object> m : data)
            {
                ((Bitmap)m.get("img")).recycle();
            }
        }catch (Exception e){}
        System.gc();
    }

    //根据id获取Paper
    private void getPaper()
    {
        new HttpGet(getString(R.string.root_url)+"/Paper", id, null,urlHandler.obtainMessage()).start();
    }

    //根据Paper的Image_FullUri获取页面图片
    private void setListData()
    {
        if(paper==null)
            return;
        String[] urls = paper.getImages();
        if(urls==null)
            return;
        data.clear();
        for(String s : urls)
            data.add(new HashMap<String, Object>());
        num = 0;
        isConnect = true;
        for(int i=0;i<urls.length;i++) {
            new HttpBitmap(getString(R.string.root_resource)+ urls[i],handler.obtainMessage(-1,i,0)).start();
        }
    }
}

