package com.xilingyuli.libraryapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class MyService extends Service {

    private Intent startIntent = null;
    private BroadcastReceiver receiver = null;
    private SharedPreferences sharedPreferences = null;
    private DateFormat dateFormat = null;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpURLConnection.HTTP_OK) {
                Bundle bd = msg.getData();
                if (bd.containsKey("Notification"))
                {
                    try{
                        Document doc = XmlDeal.getXml(bd.getString("Notification"));
                        Element root = doc.getDocumentElement();
                        NodeList nl = root.getChildNodes();
                        if(!sharedPreferences.contains(nl.item(1).getTextContent())
                                && System.currentTimeMillis() - dateFormat.parse(nl.item(2).getTextContent()).getTime() <= 2*24*3600*1000 ) {
                            Notification notification = new Notification.Builder(MyService.this)
                                    .setContentTitle("新消息")
                                    .setContentText(nl.item(0).getTextContent())
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentIntent(PendingIntent.getActivity(MyService.this, 0, new Intent(MyService.this, LoginCoverActivity.class), 0)) // 关联PendingIntent
                                    .build();
                            notification.flags |= Notification.FLAG_AUTO_CANCEL;
                            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(228, notification);
                            sharedPreferences.edit().putString(nl.item(1).getTextContent(),nl.item(2).getTextContent()).apply();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        flags = START_STICKY;
        super.onStartCommand(intent,flags,startId);

        if(dateFormat==null)
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        if(sharedPreferences==null){
            sharedPreferences = getSharedPreferences("Notification",Context.MODE_PRIVATE);
            for(String key : sharedPreferences.getAll().keySet())
            {
                try {
                    long date = dateFormat.parse(sharedPreferences.getString(key,null)).getTime();
                    if(System.currentTimeMillis() - date > 2*24*3600*1000)
                        sharedPreferences.edit().remove(key).apply();
                }catch (Exception e){
                    sharedPreferences.edit().remove(key).apply();
                }
            }
        }

        if(startIntent==null)
            startIntent=intent;

        if(receiver==null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_SCREEN_ON);
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    new HttpGet(getString(R.string.root_url),"Notification","newest=true",handler.obtainMessage()).start();
                }
            };
            registerReceiver(receiver, filter);
        }

        return flags;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return  null;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        this.stopForeground(true);
        if(startIntent!=null){
            startService(startIntent);
        }
        super.onDestroy();
    }

}
