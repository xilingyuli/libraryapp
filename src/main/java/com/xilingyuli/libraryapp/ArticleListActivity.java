package com.xilingyuli.libraryapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleListActivity extends AppCompatActivity {

    private ListView listView;
    private String sourceXml;
    private Paper[] papers;
    private String[] xmls;
    private List<Map<String,String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);
        listView = (ListView)findViewById(R.id.listView);
        View emptyView = findViewById(R.id.empty);
        listView.setEmptyView(emptyView);

        sourceXml = getIntent().getCharSequenceExtra("ArrayOfPaper")+"";
        papers = Paper.decodePapersFromXml(sourceXml);
        xmls = Paper.splitPapersFromXml(sourceXml);
        data = new ArrayList<Map<String, String>>();
        for(Paper p : papers)
        {
            Map<String,String> map = new HashMap<String, String>();
            map.put("title", p.getTitle());
            map.put("authors", "作者："+Arrays.toString(p.getAuthors()).replace("[","").replace("]", ""));
            map.put("paperClass","栏目："+p.getPaperClass());
            map.put("keyWords", "关键词："+Arrays.toString(p.getKeyWords()).replace("[","").replace("]", ""));
            data.add(map);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ArticleListActivity.this, ArticleViewActivity.class).putExtra("Paper", xmls[i]);
                startActivity(intent);
            }
        });

        listView.setAdapter(new SimpleAdapter(
                ArticleListActivity.this,
                ArticleListActivity.this.data,
                R.layout.listitem_article,
                new String[]{"title","authors","paperClass","keyWords"},
                new int[]{R.id.textView,R.id.textView2,R.id.textView3,R.id.textView4}) {
        });
    }

}
