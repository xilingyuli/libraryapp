package com.xilingyuli.libraryapp;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by xilingyuli on 2015/12/10.
 * 对应后台volume，表示一期期刊
 */
public class Volume {
    private String id,coverImage,publishDate,totalVolume,yearVolume;
    Volume(String id) { this.id = id; }
    public String getId() { return this.id; }
    public void setCoverImage(String coverImage) { this.coverImage = coverImage; }
    public String getCoverImage() { return this.coverImage; }
    public void setPublishDate(String publishDate) { this.publishDate = publishDate; }
    public String getPublishDate() { return this.publishDate; }
    public void setTotalVolume(String totalVolume) { this.totalVolume = totalVolume; }
    public String getTotalVolume() { return this.totalVolume; }
    public void setYearVolume(String yearVolume) { this.yearVolume = yearVolume; }
    public String getYearVolume() { return this.yearVolume; }

    //根据xml字符串解析Volume对象
    public static Volume decodeFromXml(String xml)
    {
        Document doc = XmlDeal.getXml(xml);
        if(doc==null)
            return null;
        Element root = doc.getDocumentElement();
        return decodeFromXml(root);
    }

    //根据根节点解析Volume对象
    public static Volume decodeFromXml(Element root) {
        if (root == null || !root.getTagName().equals("Volume"))
            return null;
        NodeList nl = root.getChildNodes();

        Volume volume = new Volume(nl.item(1).getTextContent());
        volume.setPublishDate(nl.item(2).getTextContent());
        volume.setTotalVolume(nl.item(3).getTextContent());
        volume.setYearVolume(nl.item(4).getTextContent());
        NodeList img = ((Element)nl.item(0)).getElementsByTagName("FullUri");
        if(img.getLength()>0)
            volume.setCoverImage(img.item(0).getTextContent());
        return volume;
    }

    //根据xml字符串解析Volume对象数组
    public static Volume[] decodeVolumesFromXml(String xml)
    {
        Document doc = XmlDeal.getXml(xml);
        if(doc==null)
            return null;
        Element root = doc.getDocumentElement();
        NodeList nl = root.getElementsByTagName("Volume");
        Volume[] volumes = new Volume[nl.getLength()];
        for(int i=0;i<nl.getLength();i++)
            volumes[i] = decodeFromXml((Element)nl.item(i));
        return volumes;
    }
}
