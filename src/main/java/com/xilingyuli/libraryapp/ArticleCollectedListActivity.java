package com.xilingyuli.libraryapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleCollectedListActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;  //收藏夹
    private ListView listView;
    private List<Map<String,String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_collected_list);
        listView = (ListView)findViewById(R.id.listView);
        View emptyView = findViewById(R.id.empty);
        listView.setEmptyView(emptyView);
        sharedPreferences = getSharedPreferences("collect", Context.MODE_PRIVATE);
        data = new ArrayList<Map<String, String>>();
        for(String key : sharedPreferences.getAll().keySet())
        {
            Map<String,String> map = new HashMap<String,String>();
            map.put("id",key);
            map.put("title",sharedPreferences.getString(key,""));
            data.add(map);
        }

        //单击跳转
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ArticleCollectedListActivity.this, ArticleViewActivity.class).putExtra("Id", data.get(i).get("id"));
                startActivity(intent);
            }
        });

        //长按删除
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ArticleCollectedListActivity.this);
                builder.setMessage("确定删除该条收藏？");
                builder.setTitle("取消收藏");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        sharedPreferences.edit().remove(data.get(i).get("id")).apply();
                        data.remove(i);
                        ((SimpleAdapter)listView.getAdapter()).notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("取消",null);
                builder.show();
                return true;
            }
        });

        listView.setAdapter(new SimpleAdapter(
                ArticleCollectedListActivity.this,
                ArticleCollectedListActivity.this.data,
                R.layout.listitem_article_only_title,
                new String[]{"title"},
                new int[]{R.id.textView}));
    }
}
